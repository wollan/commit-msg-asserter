#!/usr/bin/env python
# Rules from https://chris.beams.io/posts/git-commit/

import os
import sys
from subprocess import call

def main(args):
    
    rules = [
        (1, check_rule_one, "Separate subject from body with a blank line"),
        (2, check_rule_two, "Limit the subject line to 50 characters"),
        (3, check_rule_three, "Capitalize the subject line"),
        (4, check_rule_four, "Do not end the subject line with a period"),
        (5, check_rule_five, "Use the imperative mood in the subject line"),
        (6, check_rule_six, "Wrap the body at 72 characters"),
        (7, check_rule_seven, "Use the body to explain what and why vs. how"),
    ]
    
    if len(args) != 2:
        return (-1, "USAGE: {} <commit-msg>".format(args[0]))

    if is_merge_commit():
        return (0, "") # skip merge commits
    
    commit_msg = strip_comments(args[1])

    for (rule_no, check, desc) in rules:
        ok = check(commit_msg)
        if not ok:
            return (rule_no, "Violation of rule #{} - {}\n\n{}".format(rule_no, desc, commit_msg))

    return (0, None) # success!

def check_rule_one(commit_msg):
    lines = commit_msg.split('\n')
    if len(lines) <= 1:
        return True

    if len(lines) == 2:
        return False # just two lines are forbidden

    return len(lines[1]) == 0 and len(lines[2]) > 0 

def check_rule_two(commit_msg):
    subject_line = commit_msg.split('\n')[0]
    subject_len = len(subject_line)
    return subject_len >= 2 and subject_len <= 50

def check_rule_three(commit_msg):
    return commit_msg[0].upper() == commit_msg[0]

def check_rule_four(commit_msg):
    subject_line = commit_msg.split('\n')[0]
    return not subject_line.endswith('.')

def check_rule_five(commit_msg):
    return True # no way of determine imperative mood as of now

def check_rule_six(commit_msg):
    lines = commit_msg.split('\n')
    for line in lines[2:]:
        if len(line) > 72:
            return False

    return True 

def check_rule_seven(commit_msg):
    return True # cannot analyze text content as of now

def is_merge_commit():
    devnull = open(os.devnull, 'w')
    code = call(["git", "rev-parse", "-q", "--verify", "MERGE_HEAD"],
            stdout=devnull, stderr=devnull)
    return code == 0

def strip_comments(commit_msg):
    lines = commit_msg.split('\n')
    nocomments = filter(lambda x: len(x) == 0 or x[0] != '#', lines)
    return '\n'.join(nocomments)

if __name__ == '__main__':
    (exit_code, output) = main(sys.argv)
    if output is not None:
        print(output)
    
    exit(exit_code)
