#!/usr/bin/env python

import unittest
from assert_commit_msg import main

class TestMain(unittest.TestCase):

    def test_noArgs_codeMinusOne(self):
        (exit_code, output) = main(['file.py'])
        self.assertEqual(exit_code, -1)

    def test_twoArgs_codeMinusOne(self):
        (exit_code, output) = main(['file.py', 'a', 'b'])
        self.assertEqual(exit_code, -1)

    def test_rule1_twoLinesNoSeparation_codeOne(self):
        (exit_code, output) = main(['file.py', 
            "Add button\nIt is blue."])
        self.assertEqual(exit_code, 1)

    def test_rule1_threeLinesWithSeparation_codeZero(self):
        (exit_code, output) = main(['file.py', 
            "Add button\n\nIt is blue."])
        self.assertEqual(exit_code, 0)

    def test_rule1_threeLinesWithoutSeparation_codeOne(self):
        (exit_code, output) = main(['file.py', 
            "Add button\nA blue one.\nIt is blue."])
        self.assertEqual(exit_code, 1)

    def test_rule1_fourLinesDoubleSeparation_codeOne(self):
        (exit_code, output) = main(['file.py', 
            "Add button\n\n\nIt is blue."])
        self.assertEqual(exit_code, 1)

    def test_rule1_fourLinesDoubleSeparationOneSeparationCommented_codeZero(self):
        (exit_code, output) = main(['file.py', 
            "Add button\n#\n\nIt is blue."])
        self.assertEqual(exit_code, 0)

    def test_rule2_emptySubject_codeTwo(self):
        (exit_code, output) = main(['file.py', 
            "\n\nIt is blue."])
        self.assertEqual(exit_code, 2)

    def test_rule2_singleCharSubject_codeTwo(self):
        (exit_code, output) = main(['file.py', 
            "Q\n\nIt is blue."])
        self.assertEqual(exit_code, 2)

    def test_rule2_doubleCharSubject_codeZero(self):
        (exit_code, output) = main(['file.py', 
            "QQ\n\nIt is blue."])
        self.assertEqual(exit_code, 0)

    def test_rule2_51CharSubject_codeTwo(self):
        (exit_code, output) = main(['file.py', 
            "123456789012345678901234567890123456789012345678901"])
        self.assertEqual(exit_code, 2)

    def test_rule2_50CharSubject_codeTwo(self):
        (exit_code, output) = main(['file.py', 
            "12345678901234567890123456789012345678901234567890"])
        self.assertEqual(exit_code, 0)

    def test_rule3_capitalFirstSubjectLetter_codeZero(self):
        (exit_code, output) = main(['file.py', 
            "Add button"])
        self.assertEqual(exit_code, 0)

    def test_rule3_lowercaseFirstSubjectLetter_codeThree(self):
        (exit_code, output) = main(['file.py', 
            "add button"])
        self.assertEqual(exit_code, 3)

    def test_rule4_subjectEndsWithPeriod_codeFour(self):
        (exit_code, output) = main(['file.py', 
            "Add button."])
        self.assertEqual(exit_code, 4)

    def test_rule4_subjectDoesntEndWithPeriod_codeFour(self):
        (exit_code, output) = main(['file.py', 
            "Add button"])
        self.assertEqual(exit_code, 0)

    def test_rule6_bodyWithA73CharLine_codeSix(self):
        (exit_code, output) = main(['file.py', 
            "Add button\n\n1234567890123456789012345678901234567890123456789012345678901234567890123"])
        self.assertEqual(exit_code, 6)

if __name__ == '__main__':
    unittest.main()
