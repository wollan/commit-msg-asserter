#!/bin/bash -e

TARGET="../.git/hooks/commit-msg"
cd "$(dirname $0)"

if [[ -f "$TARGET" ]]
then
    echo ".git/hooks/commit-msg already exists! - aborting install to not overwrite existing hook..." 
    exit 1
fi

cp commit-msg-local "$TARGET"
echo "local commit-msg hook successfully installed!"
