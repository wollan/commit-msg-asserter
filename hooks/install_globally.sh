#!/bin/bash -e

DIR="$HOME/.git/hooks"
TARGET="$DIR/commit-msg"
HOOK_PATH=$(git config --global --get core.hooksPath || :)
FORCE="$1"
cd "$(dirname $0)"

if [[ "$HOOK_PATH" != "" && "$FORCE" != "--force" ]]
then
    echo "Global git hook path is already configured with value '$HOOK_PATH', aborting script. (run 'git config --global --unset core.hooksPath' to remove setting)"  
    exit 1
fi

if [[ -f "$TARGET" && "$FORCE" != "--force" ]]
then
    echo "$TARGET already exists - will just update the assert script, not the hook (to not override any manual changes)" 
else
    mkdir -p "$DIR"
    cp commit-msg-global "$TARGET"
fi

cp ../assert_commit_msg.py "$DIR/"
git config --global core.hooksPath "$DIR"

echo "global commit-msg hook successfully installed!"
