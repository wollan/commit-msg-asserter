## Commit message asserter

Asserts commit messages according to https://chris.beams.io/posts/git-commit/

#### Prereqs

1. Python 2 or 3

#### Install in single repo

1. Clone this repo
1. Copy `assert_commit_msg.py` and and `hooks` dir into the root 
of your git repository
1. Run `hooks/install_locally.sh` in your git repo
1. Add `assert_commit_msg.py` and `hooks` dir to your repo (or gitignore them)

To update script, pull this repo and copy `assert_commit_msg.py` again.

#### Install globally

1. Clone this repo
1. Run `hooks/install_globally.sh` 

To update script, pull this repo and run `hooks/install_globally.sh --force`.
